let config = {
  'dir': {
    'src': 'src/',
    'build': 'dist/',
    'blocks': 'src/blocks/'
  }
};

module.exports = config;
$(document).ready(function(){

  /**
   * Turn on svg4everybody
   */
  svg4everybody();



  /**
   * Show and close mobile menu
   */

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
    $('body').removeClass('s-prevent-scroll');
  });



  /**
   * Show more seo text in about section
   */
  $('.s-about-company__show-more-btn').on('click', function () {
    var button = $(this);
    var text = button.text() == 'Показать больше' ? 'Показать меньше' : 'Показать больше';
    button.text(text);
  });



  /**
  * Swiper Sliders
  */

  var promoSlider = new Swiper('.s-promo__slider .swiper-container');

  var productsSlider = new Swiper('.s-products__slider .swiper-container', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.s-products__slider-nav .s-slider-nav__btn--next',
      prevEl: '.s-products__slider-nav .s-slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });

  var servicesSlider = new Swiper('.s-services__slider .swiper-container', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.s-services__slider-nav .s-slider-nav__btn--next',
      prevEl: '.s-services__slider-nav .s-slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });

  var productionSlider = new Swiper('.s-production__slider .swiper-container', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.s-production__slider-nav .s-slider-nav__btn--next',
      prevEl: '.s-production__slider-nav .s-slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });

  var certificatesSlider = new Swiper('.s-certificates__slider .swiper-container', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.s-certificates__slider-nav .s-slider-nav__btn--next',
      prevEl: '.s-certificates__slider-nav .s-slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });



  /**
  * Lightgallery
  */

  $('.s-certificates__slider').lightGallery({
    selector: '.s-certificates__thumb',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  $('.s-production__slider').lightGallery({
    selector: '.s-production__thumb',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });
});